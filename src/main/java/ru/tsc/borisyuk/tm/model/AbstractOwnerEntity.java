package ru.tsc.borisyuk.tm.model;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
