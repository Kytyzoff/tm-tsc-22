package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Task findByName(String userId, String name);

    Task removeByName(String userId, String name);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task startById(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);
    Task findByProjectAndTaskId(String userId, String projectId, String taskId);

}
