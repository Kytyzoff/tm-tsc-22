package ru.tsc.borisyuk.tm.component;

import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.repository.IUserRepository;
import ru.tsc.borisyuk.tm.api.service.*;
import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.command.user.*;
import ru.tsc.borisyuk.tm.constant.TerminalConst;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.system.UnknownCommandException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.repository.CommandRepository;
import ru.tsc.borisyuk.tm.repository.ProjectRepository;
import ru.tsc.borisyuk.tm.repository.TaskRepository;
import ru.tsc.borisyuk.tm.repository.UserRepository;
import ru.tsc.borisyuk.tm.service.*;
import ru.tsc.borisyuk.tm.command.task.*;
import ru.tsc.borisyuk.tm.command.project.*;
import ru.tsc.borisyuk.tm.command.system.*;

import java.util.Date;
import java.util.Scanner;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initRegistry();
        initData();
        runArgs(args);
        logService.debug("Test environment.");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        /*projectService.add(new Project("Project 1", "1", new Date(2003,01,01)));
        projectService.changeStatusByName("Project 1", Status.COMPLETED);
        projectService.add(new Project("Project 2", "2", new Date(2002,01,01)));
        projectService.add(new Project("Project 3", "3", new Date(2001,01,01)));
        projectService.changeStatusByName("Project 3", Status.IN_PROGRESS);
        taskService.add(new Task("Task a", "1", new Date(03,01,01)));
        taskService.changeStatusByName("Task a", Status.COMPLETED);
        taskService.add(new Task("Task b", "2", new Date(02,01,01)));
        taskService.add(new Task("Task c", "3", new Date(01,01,01)));
        taskService.changeStatusByName("Task c", Status.IN_PROGRESS);*/
    }

    private void initRegistry() {
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new DeveloperInfoShowCommand());
        registry(new ArgumentsShowCommand());
        registry(new CommandsShowCommand());
        registry(new InfoShowCommand());
        registry(new VersionShowCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectRemoveWithAllTasksById());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskIsBindToProjectByIdCommand());
        registry(new TaskIsUnbindFromProjectByIdCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveAllFromProjectByIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowAllFromProjectByIdCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserByLoginLockCommand());
        registry(new UserByLoginUnlockCommand());
        registry(new UserByLoginRemoveCommand());
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownCommandException();
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IAuthService getAuthService() { return authService; }

    @Override
    public IUserService getUserService() { return userService; }

}
