package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void clear();

    E findById(String id);

    E findByIndex(Integer index);

    E removeById(String id);

    E removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

}
