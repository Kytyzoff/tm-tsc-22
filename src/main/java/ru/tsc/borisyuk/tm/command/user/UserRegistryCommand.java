package ru.tsc.borisyuk.tm.command.user;

import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-reg";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "user registry...";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
