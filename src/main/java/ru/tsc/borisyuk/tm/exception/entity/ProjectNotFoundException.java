package ru.tsc.borisyuk.tm.exception.entity;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
