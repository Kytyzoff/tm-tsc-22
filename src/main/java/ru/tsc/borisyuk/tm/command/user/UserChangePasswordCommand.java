package ru.tsc.borisyuk.tm.command.user;

import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "change user's password...";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
