package ru.tsc.borisyuk.tm.repository;

import ru.tsc.borisyuk.tm.api.repository.IOwnerRepository;
import ru.tsc.borisyuk.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void add(final String userId, final E entity) {
        if (userId.equals(entity.getUserId())) {
            entities.add(entity);
        }
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (userId.equals(entity.getUserId()))
            entities.remove(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());

    }

    @Override
    public List<E> findAll(final String userId, Comparator<E> comparator) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void clear(final String userId) {
        final List<E> listOfProjects = findAll(userId);
        entities.removeAll(listOfProjects);
    }

    @Override
    public E findById(final String userId, final String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .filter(e -> e.getUserId().equals(userId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> listOfEntities = findAll(userId);
        return listOfEntities.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        return entity != null;
    }

}
