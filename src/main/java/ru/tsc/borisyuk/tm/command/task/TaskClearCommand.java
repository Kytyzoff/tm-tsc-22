package ru.tsc.borisyuk.tm.command.task;

import ru.tsc.borisyuk.tm.command.AbstractTaskCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove all tasks...";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
