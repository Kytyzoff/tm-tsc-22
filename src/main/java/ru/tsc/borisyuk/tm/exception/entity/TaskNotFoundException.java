package ru.tsc.borisyuk.tm.exception.entity;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
