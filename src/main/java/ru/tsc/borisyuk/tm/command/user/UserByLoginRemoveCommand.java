package ru.tsc.borisyuk.tm.command.user;

import ru.tsc.borisyuk.tm.command.AbstractCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class UserByLoginRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "remove user by login...";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}