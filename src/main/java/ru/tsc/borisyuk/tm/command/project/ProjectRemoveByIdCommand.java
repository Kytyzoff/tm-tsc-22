package ru.tsc.borisyuk.tm.command.project;

import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id...";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("[REMOVE PROJECT BY ID]");
        serviceLocator.getProjectService().removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
