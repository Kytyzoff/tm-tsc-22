package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;

public interface IUserService extends IService<User> {

    User findByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User findByLogin(String login);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
