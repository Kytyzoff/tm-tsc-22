package ru.tsc.borisyuk.tm.command.project;

import ru.tsc.borisyuk.tm.command.AbstractProjectCommand;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change status by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusById(userId, id, status);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
